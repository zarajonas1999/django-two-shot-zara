# Generated by Django 5.0 on 2023-12-18 08:00

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0005_alter_receipt_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(null=True),
        ),
    ]

from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Receipt, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm

# Create your views here.

# Create a view that will get all of the instances of the Receipt model
# and put them in the context for the template.
# Register to reciepts.url as path '' and name 'home'

@login_required #this decorator protects the render url with a login requirement
def show_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user) # filters the view to only show logged in user's purchases
    context = {
        'receipts': receipts,
        }
    return render(request, 'receipts/list.html', context)

def redirect_to_show_receipts(request):  # import to project similar to include
    return redirect('home')              # but to this redirect's argument in particular
                                         # this argument is from this app's url py file
                                         # which is tied to the show_receipts view above

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
            'form': form,
            }

    return render(request, 'receipts/create.html', context)
